<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Auth\LoginController@showLoginForm');


Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->middleware('verified');
Route::group(['middleware' => ['auth']], function () {
    Route::resource('users', 'UserController');
    Route::get('profile', 'UserController@editProfile');
    Route::post('update-profile', 'UserController@updateProfile');
    Route::resource('mes', 'MesController');
    Route::resource('gerentes', 'GerenteController');
    Route::resource('operaciones', 'OperacionesController');
    Route::resource('proyectos', 'ProyectoController');
    Route::resource('regions', 'RegionController');
    Route::post('Carga', 'OperacionesController@carga')->name('carga');
});


