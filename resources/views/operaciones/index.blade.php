@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Operaciones</h1>
        <h1 class="pull-right">
            <button type="button" class="btn btn-primary pull-right" data-toggle="modal" style="margin-top: -10px;margin-bottom: 5px"
                    data-target="#basicExampleModal">
                Añadir Operaciones
            </button>
            <!-- Modal añadir-->
            <div class="modal fade" id="basicExampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Añadir objetivo</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        {!! Form::open(['route' => 'carga', 'method' => 'post',  'enctype' => 'multipart/form-data']) !!}
                        <div class="modal-body">
                            @include('operaciones.fields')
                        </div>
                        <div class="modal-footer">
                            <!-- Submit Field -->
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <!--termina modal-->
           <!--<a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{{ route('operaciones.create') }}">Add New</a>-->
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('operaciones.table')
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

