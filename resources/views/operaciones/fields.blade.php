<!-- Mes Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mes', 'Mes:') !!}
    {!! Form::number('mes', null, ['class' => 'form-control']) !!}
</div>

<!-- Año Field -->
<div class="form-group col-sm-6">
    {!! Form::label('año', 'Año:') !!}
    {!! Form::number('año', null, ['class' => 'form-control','id'=>'año']) !!}
</div>


<!-- Id Proyecto Field -->
<div class="form-group col-sm-12">
    {!! Form::label('id_proyecto', 'Numero de Estacionamiento:') !!}
    {!! Form::number('id_proyecto', null, ['class' => 'form-control']) !!}
</div>

<!-- Excel -->
<div class="form-group col-sm-12">
    {!! Form::label('excel', 'Excel:') !!}
    {!! Form::file('archivo',  $attributes = ['accept'=>"application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"]) !!}
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('operaciones.index') }}" class="btn btn-default">Cancelar</a>
</div>
