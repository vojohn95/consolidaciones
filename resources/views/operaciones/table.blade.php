@section('css')
    @include('layouts.datatables_css')
@endsection

<table id="example" class="table table-striped table-bordered" style="width:100%">
    <thead>
    <tr>
        <th>Día</th>
        <th>Operaciones 2020</th>
        <th>Operaciones 2019</th>
        <th>Variación</th>
        <th>Variación %</th>
        <th>Mes</th>
        <th>Año</th>
        <th>Estacionamiento</th>
        <th colspan="2">Opciones</th>
    </tr>
    </thead>
    <tbody>
    @foreach($operaciones as $proyecto)
        <tr>
            <td>{!! $proyecto->dia !!}</td>
            <td>{!! $proyecto->no_operaciones !!}</td>
            <td>{!! $proyecto->ope_pasada !!}</td>
            <td>{!! $proyecto->no_operaciones - $proyecto->ope_pasada !!}</td>
            <td>{!! round(($proyecto->no_operaciones - $proyecto->ope_pasada)/$proyecto->ope_pasada,2) !!}</td>
            <td>{!! $proyecto->mes !!}</td>
            <td>{!! $proyecto->ano !!}</td>
            <td>({!! $proyecto->no_proyecto !!}) {!! $proyecto->Nombre !!}</td>
            <td>
                {!! Form::open(['route' => ['operaciones.destroy', $proyecto->id], 'method' => 'delete' ]) !!}
                <a type="button" class="btn-floating btn-sm btn-primary" href="{!! route('operaciones.edit',[$proyecto->id]) !!}">Editar</a>
                {!! Form::button('Eliminar', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('¿Estas seguro?')"]) !!}
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <th>Día</th>
        <th>Operaciones 2020</th>
        <th>Operaciones 2019</th>
        <th>Variación</th>
        <th>Variación %</th>
        <th>Mes</th>
        <th>Año</th>
        <th>Estacionamiento</th>
        <th colspan="2">Opciones</th>
    </tr>
    </tfoot>
</table>
{{$operaciones->links()}}
@push('scripts')
    @include('layouts.datatables_js')

    <script>
        $(document).ready(function () {
            $('#example').DataTable();
        });
    </script>
@endpush
