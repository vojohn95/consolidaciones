<!-- Dia Field -->
<div class="form-group">
    {!! Form::label('dia', 'Dia:') !!}
    <p>{{ $operaciones->dia }}</p>
</div>

<!-- No Operaciones Field -->
<div class="form-group">
    {!! Form::label('no_operaciones', 'No Operaciones:') !!}
    <p>{{ $operaciones->no_operaciones }}</p>
</div>

<!-- Ope Pasada Field -->
<div class="form-group">
    {!! Form::label('ope_pasada', 'Ope Pasada:') !!}
    <p>{{ $operaciones->ope_pasada }}</p>
</div>

<!-- Mes Field -->
<div class="form-group">
    {!! Form::label('mes', 'Mes:') !!}
    <p>{{ $operaciones->mes }}</p>
</div>

<!-- Año Field -->
<div class="form-group">
    {!! Form::label('año', 'Año:') !!}
    <p>{{ $operaciones->año }}</p>
</div>

<!-- Id Proyecto Field -->
<div class="form-group">
    {!! Form::label('id_proyecto', 'Id Proyecto:') !!}
    <p>{{ $operaciones->id_proyecto }}</p>
</div>

