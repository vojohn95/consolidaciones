@section('css')
    @include('layouts.datatables_css')
@endsection

<table id="example" class="table table-striped table-bordered" style="width:100%">
    <thead>
    <tr>
        <th>Gerente</th>
        <th>Email</th>
        <th>No. Proyecto</th>
        <th>Proyecto</th>
        <th>Registrado desde</th>
        <th colspan="2">Opciones</th>
    </tr>
    </thead>
    <tbody>
    @foreach($gerentes as $gerente)
        <tr>
            <td>{!! $gerente->Nombre !!}</td>
            <td>{!! $gerente->Email !!}</td>
            <td>{!! $gerente->No_proyecto !!}</td>
            <td>{!! $gerente->ProyectoNombre !!}</td>
            <td>{!! $gerente->created_at !!}</td>
            <td>
                {!! Form::open(['route' => ['gerentes.destroy', $gerente->id], 'method' => 'delete' ]) !!}
                <a type="button" class="btn-floating btn-sm btn-primary" href="{!! route('gerentes.edit',[$gerente->id]) !!}">Editar</a>
                {!! Form::button('Eliminar', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('¿Estas seguro?')"]) !!}
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <th>Gerente</th>
        <th>Email</th>
        <th>No. Proyecto</th>
        <th>Proyecto</th>
        <th>Registrado desde</th>
        <th colspan="2">Opciones</th>
    </tr>
    </tfoot>
</table>
{{$gerentes->links()}}
@push('scripts')
    @include('layouts.datatables_js')

    <script>
        $(document).ready(function () {
            $('#example').DataTable();
        });
    </script>
@endpush
