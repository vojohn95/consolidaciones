<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('Nombre', 'Nombre:') !!}
    <p>{{ $gerente->Nombre }}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('Email', 'Email:') !!}
    <p>{{ $gerente->Email }}</p>
</div>

<!-- Password Field -->
<div class="form-group">
    {!! Form::label('Password', 'Password:') !!}
    <p>{{ $gerente->Password }}</p>
</div>

<!-- Id Proyecto Field -->
<div class="form-group">
    {!! Form::label('id_proyecto', 'Id Proyecto:') !!}
    <p>{{ $gerente->id_proyecto }}</p>
</div>

