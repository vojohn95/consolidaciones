@section('css')
    @include('layouts.datatables_css')
@endsection

<div class="table-responsive">
    <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
        <tr>
            <th>Grupo</th>
            <th>Operaciones 2020</th>
            <th>Operaciones 2019</th>
            <th>Variación</th>
            <th>Variación %</th>
        </tr>
        </thead>
        <tbody>
        @foreach($estacionamientos as $estacionamiento)
            <tr>
                <td>Central</td>
                <td>{!! $estacionamiento->operacion !!}</td>
                <td>{!! $estacionamiento->Pasado !!}</td>
                @if($estacionamiento->operacion - $estacionamiento->Pasado > 0)
                    <td style="color: green;">{!! $estacionamiento->operacion - $estacionamiento->Pasado  !!}</td>
                @else
                    <td style="color: red;">{!! $estacionamiento->operacion - $estacionamiento->Pasado  !!}</td>
                @endif
                @if(round((($estacionamiento->operacion - $estacionamiento->Pasado)/$estacionamiento->Pasado)*100,2) > 0)
                    <td style="color: green;">{!!round((($estacionamiento->operacion - $estacionamiento->Pasado)/$estacionamiento->Pasado)*100,2)   !!}</td>
                @else
                    <td style="color: red;">{!!round((($estacionamiento->operacion - $estacionamiento->Pasado)/$estacionamiento->Pasado)*100,2)   !!}</td>
                @endif
            </tr>
        @endforeach

        </tbody>
        <tfoot>
        <tr>
            <th>Grupo</th>
            <th>Operaciones 2020</th>
            <th>Operaciones 2019</th>
            <th>Variación</th>
            <th>Variación %</th>
        </tr>
        </tfoot>
    </table>
</div>


@push('scripts')
    @include('layouts.datatables_js')

    <script>
        $(document).ready(function () {
            $('#example').DataTable();
        });
    </script>
@endpush
