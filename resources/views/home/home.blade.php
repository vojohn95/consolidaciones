@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Desglose</h1>
        <!--<h1 class="pull-right">
            <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px"
               href="">Añadir nuevo</a>
        </h1>-->
    </section>
    <div class="content">
        <div class="clearfix"></div>
        @include('flash::message')
        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                @include('home.table_info')
                <!-- Columns are always 50% wide, on mobile and desktop -->
                    <div class="row">
                        @include('home.table_semana2')
                    </div>

            </div>
        </div>
    </div>
@endsection

