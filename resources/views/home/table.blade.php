@section('css')
    @include('layouts.datatables_css')
@endsection

<table id="example" class="table table-striped table-bordered" style="width:100%">
    <thead>
    <tr>
        <th>Grupo</th>
        <th>Mes</th>
        <th>Dias [1 al 15]</th>
    </tr>
    </thead>
    <tbody>
    @foreach($semanas1 as $estacionamiento)
        <tr>
            <td>Central</td>
            <td>{!! $estacionamiento->mes !!}</td>
            <td>{!! $estacionamiento->operacion !!}</td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <th>Grupo</th>
        <th>Mes</th>
        <th>Dias [1 al 15]</th>
    </tr>
    </tfoot>
</table>


@push('scripts')
    @include('layouts.datatables_js')

    <script>
        $(document).ready(function () {
            $('#example').DataTable();
        });
    </script>
@endpush
