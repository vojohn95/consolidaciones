@section('css')
    @include('layouts.datatables_css')
@endsection
<div class="col-xs-6">
    <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
        <tr>
            <th>Grupo</th>
            <th>Mes</th>
            <th>Dias [01 al 15]</th>
        </tr>
        </thead>
        <tbody>
        @foreach($semanas1 as $estacionamiento)
            <tr>
                <td>Central</td>
                <td>
                    @if($estacionamiento->mes == '1')
                        Enero
                    @elseif($estacionamiento->mes == '2')
                        Febrero
                    @elseif($estacionamiento->mes == '3')
                        Marzo
                    @elseif($estacionamiento->mes == '4')
                        Abril
                    @elseif($estacionamiento->mes == '5')
                        Mayo
                    @elseif($estacionamiento->mes == '6')
                        Junio
                    @elseif($estacionamiento->mes == '7')
                        Julio
                    @elseif($estacionamiento->mes == '8')
                        Agosto
                    @elseif($estacionamiento->mes == '9')
                        Septiembre
                    @elseif($estacionamiento->mes == '10')
                        Octubre
                    @elseif($estacionamiento->mes == '11')
                        Noviembre
                    @else
                        Diciembre
                    @endif
                </td>
                <td>{!! $estacionamiento->operacion !!}</td>

                @endforeach
            </tr>
        </tbody>
        <tfoot>
        <tr>
            <th>Grupo</th>
            <th>Mes</th>
            <th>Dias [01 al 15]</th>
        </tr>
        </tfoot>
    </table>
</div>

<div class="col-xs-6">
    <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
        <tr>
            <th>Dias [16 al 31]</th>
        </tr>
        </thead>
        <tbody>
        @foreach($semanas2 as $estacionamiento)
            <tr>
                <td>{!! $estacionamiento->operacion !!}</td>

                @endforeach
            </tr>
        </tbody>
        <tfoot>
        <tr>
            <th>Dias [16 al 31]</th>
        </tr>
        </tfoot>
    </table>
</div>

@push('scripts')
    @include('layouts.datatables_js')

    <script>
        $(document).ready(function () {
            $('#example').DataTable();
        });
    </script>
@endpush
