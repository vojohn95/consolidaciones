<!-- O Field -->
<div class="form-group col-sm-6">
    {!! Form::label('o', 'O:') !!}
    {!! Form::number('o', null, ['class' => 'form-control']) !!}
</div>

<!-- Mes Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Mes', 'Mes:') !!}
    {!! Form::text('Mes', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('mes.index') }}" class="btn btn-default">Cancel</a>
</div>
