@section('css')
    @include('layouts.datatables_css')
@endsection

<table id="example" class="table table-striped table-bordered" style="width:100%">
    <thead>
    <tr>
        <th>Nombre</th>
        <th>Correo</th>
        <th>Registrado desde</th>
        <th colspan="2">Opciones</th>
    </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
        <tr>
            <td>{!! $user->name !!}</td>
            <td>{!! $user->email !!}</td>
            <td>{!! $user->created_at !!}</td>
            <td>
                {!! Form::open(['route' => ['users.destroy', $user->id], 'method' => 'delete' ]) !!}
                <a type="button" class="btn-floating btn-sm btn-primary" href="{!! route('users.edit',[$user->id]) !!}">Editar</a>
                {!! Form::button('Eliminar', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('¿Estas seguro?')"]) !!}
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <th>Nombre</th>
        <th>Correo</th>
        <th>Registrado desde</th>
        <th>Opciones</th>
    </tr>
    </tfoot>
</table>
{{$users->links()}}


@push('scripts')
    @include('layouts.datatables_js')

    <script>
        $(document).ready(function () {
            $('#example').DataTable();
        });
    </script>
@endpush
