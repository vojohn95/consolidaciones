@section('css')
    @include('layouts.datatables_css')
@endsection

<table id="example" class="table table-striped table-bordered" style="width:100%">
    <thead>
    <tr>
        <th>Nombre</th>
        <th>Identificador</th>
        <th>Registrado desde</th>
        <th colspan="2">Opciones</th>
    </tr>
    </thead>
    <tbody>
    @foreach($regions as $region)
        <tr>
            <td>{!! $region->Nombre !!}</td>
            <td>{!! $region->identificador !!}</td>
            <td>{!! $region->created_at !!}</td>
            <td>
                {!! Form::open(['route' => ['regions.destroy', $region->id], 'method' => 'delete' ]) !!}
                <a type="button" class="btn-floating btn-sm btn-primary" href="{!! route('regions.edit',[$region->id]) !!}">Editar</a>
                {!! Form::button('Eliminar', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('¿Estas seguro?')"]) !!}
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <th>Nombre</th>
        <th>Identificador</th>
        <th>Registrado desde</th>
        <th colspan="2">Opciones</th>
    </tr>
    </tfoot>
</table>
{{$regions->links()}}
@push('scripts')
    @include('layouts.datatables_js')

    <script>
        $(document).ready(function () {
            $('#example').DataTable();
        });
    </script>
@endpush
