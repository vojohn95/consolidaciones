<li class="">
    <a href="{{url('/home')}}"><i class="fa fa-edit"></i><span>Desglose</span></a>
</li>

<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{{ route('users.index') }}"><i class="fa fa-edit"></i><span>Usuarios</span></a>
</li>

<!--<li class="{{ Request::is('mes*') ? 'active' : '' }}">
    <a href="{{ route('mes.index') }}"><i class="fa fa-edit"></i><span>Mes</span></a>
</li>-->

<li class="{{ Request::is('gerentes*') ? 'active' : '' }}">
    <a href="{{ route('gerentes.index') }}"><i class="fa fa-edit"></i><span>Gerentes</span></a>
</li>

<li class="{{ Request::is('operaciones*') ? 'active' : '' }}">
    <a href="{{ route('operaciones.index') }}"><i class="fa fa-edit"></i><span>Operaciones</span></a>
</li>

<li class="{{ Request::is('proyectos*') ? 'active' : '' }}">
    <a href="{{ route('proyectos.index') }}"><i class="fa fa-edit"></i><span>Proyectos</span></a>
</li>

<li class="{{ Request::is('regions*') ? 'active' : '' }}">
    <a href="{{ route('regions.index') }}"><i class="fa fa-edit"></i><span>Regiones</span></a>
</li>

