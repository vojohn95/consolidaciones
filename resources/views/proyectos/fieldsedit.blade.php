<!-- No. proyecto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('No_proyecto', 'No. Proyecto:') !!}
    {!! Form::number('no_proyecto', null, ['min' => '1', 'class' => 'form-control']) !!}
</div>

<!-- Nombre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Nombre', 'Nombre:') !!}
    {!! Form::text('Nombre', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    <label for="sel1">Regiones:</label>
    <select class="form-control" id="sel1" name="id_region">
        <option value="">Seleccione una opcion</option>
        @foreach($regiones as $region)
            <option value="{!! $region->id !!}">{!! $region->identificador !!}/{!! $region->Nombre !!}</option>
        @endforeach
    </select>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('proyectos.index') }}" class="btn btn-default">Cancelar</a>
</div>
