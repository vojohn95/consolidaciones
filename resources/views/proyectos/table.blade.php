@section('css')
    @include('layouts.datatables_css')
@endsection

<table id="example" class="table table-striped table-bordered" style="width:100%">
    <thead>
    <tr>
        <th>No. Proyecto</th>
        <th>Nombre</th>
        <th>Region</th>
        <th>Registrado desde</th>
        <th colspan="2">Opciones</th>
    </tr>
    </thead>
    <tbody>
    @foreach($proyectos as $proyecto)
        <tr>
            <td>{!! $proyecto->no_proyecto !!}</td>
            <td>{!! $proyecto->Nombre !!}</td>
            <td>{!! $proyecto->identificador !!}/{!! $proyecto->RegionNombre !!}</td>
            <td>{!! $proyecto->created_at !!}</td>
            <td>
                {!! Form::open(['route' => ['proyectos.destroy', $proyecto->id], 'method' => 'delete' ]) !!}
                <a type="button" class="btn-floating btn-sm btn-primary" href="{!! route('proyectos.edit',[$proyecto->id]) !!}">Editar</a>
                {!! Form::button('Eliminar', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('¿Estas seguro?')"]) !!}
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <th>No. Proyecto</th>
        <th>Nombre</th>
        <th>Region</th>
        <th>Registrado desde</th>
        <th colspan="2">Opciones</th>
    </tr>
    </tfoot>
</table>
{{$proyectos->links()}}
@push('scripts')
    @include('layouts.datatables_js')

    <script>
        $(document).ready(function () {
            $('#example').DataTable();
        });
    </script>
@endpush
