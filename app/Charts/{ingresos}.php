<?php

namespace App\Charts;

use ConsoleTVs\Charts\Classes\{Chartjs}\Chart;

class {ingresos} extends Chart
{
    /**
     * Initializes the chart.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
}
