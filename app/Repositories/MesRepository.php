<?php

namespace App\Repositories;

use App\Models\Mes;
use App\Repositories\BaseRepository;

/**
 * Class MesRepository
 * @package App\Repositories
 * @version April 3, 2020, 7:26 am UTC
*/

class MesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'o',
        'Mes'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Mes::class;
    }
}
