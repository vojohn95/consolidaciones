<?php

namespace App\Repositories;

use App\Models\Gerente;
use App\Repositories\BaseRepository;

/**
 * Class GerenteRepository
 * @package App\Repositories
 * @version April 3, 2020, 7:27 am UTC
*/

class GerenteRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'Nombre',
        'Email',
        'Password',
        'id_proyecto'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Gerente::class;
    }
}
