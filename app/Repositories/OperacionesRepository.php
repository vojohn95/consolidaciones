<?php

namespace App\Repositories;

use App\Models\Operaciones;
use App\Repositories\BaseRepository;

/**
 * Class OperacionesRepository
 * @package App\Repositories
 * @version May 25, 2020, 3:34 am UTC
*/

class OperacionesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'dia',
        'no_operaciones',
        'ope_pasada',
        'mes',
        'año',
        'id_proyecto'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Operaciones::class;
    }
}
