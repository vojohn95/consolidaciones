<?php

namespace App\Http\Controllers;

use App\DataTables\OperacionesDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateOperacionesRequest;
use App\Http\Requests\UpdateOperacionesRequest;
use App\Imports\OperacionesImport;
use App\Models\Operaciones;
use App\Models\Proyecto;
use App\Repositories\OperacionesRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Maatwebsite\Excel\Facades\Excel;
use Response;

class OperacionesController extends AppBaseController
{
    /** @var  OperacionesRepository */
    private $operacionesRepository;

    public function __construct(OperacionesRepository $operacionesRepo)
    {
        $this->operacionesRepository = $operacionesRepo;
    }

    /**
     * Display a listing of the Operaciones.
     *
     * @param OperacionesDataTable $operacionesDataTable
     * @return Response
     */
    public function index()
    {
        $datos = Operaciones::join('Proyecto', 'Proyecto.id','=','OperacionesDet.id_proyecto')->select('OperacionesDet.*','Proyecto.no_proyecto','Proyecto.Nombre')->
        paginate(31);

        return view('operaciones.index')
            ->with('operaciones', $datos);
    }

    /**
     * Show the form for creating a new Operaciones.
     *
     * @return Response
     */
    public function create()
    {
        return view('operaciones.create');
    }

    /**
     * Store a newly created Operaciones in storage.
     *
     * @param CreateOperacionesRequest $request
     *
     * @return Response
     */
    public function store(CreateOperacionesRequest $request)
    {
        $input = $request->all();

        return redirect(route('operaciones.index'));
    }

    /**
     * Display the specified Operaciones.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $operaciones = $this->operacionesRepository->find($id);

        if (empty($operaciones)) {
            Flash::error('Operaciones not found');

            return redirect(route('operaciones.index'));
        }

        return view('operaciones.show')->with('operaciones', $operaciones);
    }

    /**
     * Show the form for editing the specified Operaciones.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $operaciones = $this->operacionesRepository->find($id);

        if (empty($operaciones)) {
            Flash::error('Operaciones not found');

            return redirect(route('operaciones.index'));
        }

        return view('operaciones.edit')->with('operaciones', $operaciones);
    }

    /**
     * Update the specified Operaciones in storage.
     *
     * @param  int $id
     * @param UpdateOperacionesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOperacionesRequest $request)
    {
        $operaciones = $this->operacionesRepository->find($id);

        if (empty($operaciones)) {
            Flash::error('Operaciones not found');

            return redirect(route('operaciones.index'));
        }

        $operaciones = $this->operacionesRepository->update($request->all(), $id);

        Flash::success('Operaciones updated successfully.');

        return redirect(route('operaciones.index'));
    }

    /**
     * Remove the specified Operaciones from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $operaciones = $this->operacionesRepository->find($id);

        if (empty($operaciones)) {
            Flash::error('Operaciones not found');

            return redirect(route('operaciones.index'));
        }

        $this->operacionesRepository->delete($id);


        Flash::success('Operaciones deleted successfully.');

        return redirect(route('operaciones.index'));
    }

    public function carga()
    {
        $input = request()->all();
        $Est = Proyecto::where('no_proyecto','=',$input['id_proyecto'])->select('id')->first();
        $array = (new OperacionesImport)->toArray(request()->file('archivo'));
        foreach ($array[0] as $item) {
                $ope = new Operaciones;
                $ope->fill([
                    'dia' => $item['dia'],
                    'no_operaciones' => $item['total_operaciones_2020'],
                    'ope_pasada' => $item['total_operaciones_2019'],
                    'mes' => $input['mes'],
                    'ano' => $input['año'],
                    'id_proyecto' => $Est->id
                ]);
                $ope->save();
        }
        Flash::success('Operaciones Exitosamente cargadas.');
        return redirect(route('operaciones.index'));
    }
}
