<?php

namespace App\Http\Controllers;

use App\DataTables\ProyectoDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateProyectoRequest;
use App\Http\Requests\UpdateProyectoRequest;
use App\Repositories\ProyectoRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\DB;
use Response;

class ProyectoController extends AppBaseController
{
    /** @var  ProyectoRepository */
    private $proyectoRepository;

    public function __construct(ProyectoRepository $proyectoRepo)
    {
        $this->proyectoRepository = $proyectoRepo;
    }

    /**
     * Display a listing of the Proyecto.
     *
     * @param ProyectoDataTable $proyectoDataTable
     * @return Response
     */
    public function index(ProyectoDataTable $proyectoDataTable)
    {
        //$proyecto = DB::table('Proyecto')->select('id', 'Nombre', 'identificador', 'created_at')->paginate(50);
        $proyecto = DB::table('Proyecto')
            ->join('Regiones', 'Regiones.id', '=', 'Proyecto.id_region')
            ->select('Proyecto.*', 'Regiones.Nombre as RegionNombre', 'Regiones.identificador')
            ->paginate(50);

        //dd($proyecto);

        return view('proyectos.index')
            ->with('proyectos', $proyecto);
    }

    /**
     * Show the form for creating a new Proyecto.
     *
     * @return Response
     */
    public function create()
    {
        $region = DB::table('Regiones')->select('id', 'Nombre', 'identificador')->get();
        return view('proyectos.create')->with('regiones', $region);
    }

    /**
     * Store a newly created Proyecto in storage.
     *
     * @param CreateProyectoRequest $request
     *
     * @return Response
     */
    public function store(CreateProyectoRequest $request)
    {
        $input = $request->all();

        $proyecto = $this->proyectoRepository->create($input);

        Flash::success('Proyecto agregado satisfactoriamente.');

        return redirect(route('proyectos.index'));
    }

    /**
     * Display the specified Proyecto.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $proyecto = $this->proyectoRepository->find($id);

        if (empty($proyecto)) {
            Flash::error('Proyecto not found');

            return redirect(route('proyectos.index'));
        }

        return view('proyectos.show')->with('proyecto', $proyecto);
    }

    /**
     * Show the form for editing the specified Proyecto.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $proyecto = DB::table('Proyecto')
            ->join('Regiones', 'Regiones.id', '=', 'Proyecto.id_region')
            ->select('Proyecto.*', 'Regiones.id as RegionId' ,'Regiones.Nombre as RegionNombre', 'Regiones.identificador')
            ->where('Proyecto.id', '=', $id)
            ->get();
        $region = DB::table('Regiones')->select('id', 'Nombre', 'identificador')->get();

        //dd($proyecto);

        if (empty($proyecto)) {
            Flash::error('Proyecto no encontrado');

            return redirect(route('proyectos.index'));
        }

        return view('proyectos.edit')->with('proyectos', $proyecto)->with('regiones', $region);
    }

    /**
     * Update the specified Proyecto in storage.
     *
     * @param  int              $id
     * @param UpdateProyectoRequest $request
     *
     * @return Response
     */
    public function update($id)
    {

        //dd(request()->all());
        $input = \request()->validate([
            'no_proyecto' => 'required|integer',
            'Nombre' => 'required|string',
            'id_region' => ''
        ]);

        $proyecto = DB::table('Proyecto')->select('id', 'no_proyecto', 'Nombre', 'id_region')->where('id', '=', $id)->get();

        if (empty($proyecto)) {
            Flash::error('Proyecto no encontrado');

            return redirect(route('proyectos.index'));
        }

        if ($input['no_proyecto'] != null) {
            $up = DB::table('Proyecto')
                ->where('id', '=', $id)
                ->update(['no_proyecto' => $input['no_proyecto']]);
        }
        if ($input['Nombre'] != null) {
            $up = DB::table('Proyecto')
                ->where('id', '=', $id)
                ->update(['Nombre' => $input['Nombre']]);
        }
        if ($input['id_region'] != null) {
            $up = DB::table('Proyecto')
                ->where('id', '=', $id)
                ->update(['id_region' => $input['id_region']]);
        }

        Flash::success('Proyecto actualizado correctamente.');

        return redirect(route('proyectos.index'));
    }

    /**
     * Remove the specified Proyecto from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $proyecto = DB::table('Proyecto')
            ->where('id', '=', $id)
            ->first();

        if (empty($proyecto)) {
            Flash::error('Proyecto no encontrado');

            return redirect(route('proyectos.index'));
        }

        $sql = DB::table('Proyecto')->where('id','=',$id)->delete();

        Flash::success('Proyecto eliminado correctamente.');

        return redirect(route('proyectos.index'));
    }
}
