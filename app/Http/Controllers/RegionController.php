<?php

namespace App\Http\Controllers;

use App\DataTables\RegionDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateRegionRequest;
use App\Http\Requests\UpdateRegionRequest;
use App\Repositories\RegionRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Support\Facades\DB;


class RegionController extends AppBaseController
{
    /** @var  RegionRepository */
    private $regionRepository;

    public function __construct(RegionRepository $regionRepo)
    {
        $this->regionRepository = $regionRepo;
    }

    /**
     * Display a listing of the Region.
     *
     * @param RegionDataTable $regionDataTable
     * @return Response
     */
    public function index(RegionDataTable $regionDataTable)
    {
        $region = DB::table('Regiones')->select('id', 'Nombre', 'identificador', 'created_at')->paginate(50);
        //dd($user);
        return view('regions.index')
            ->with('regions', $region);
    }

    /**
     * Show the form for creating a new Region.
     *
     * @return Response
     */
    public function create()
    {
        return view('regions.create');
    }

    /**
     * Store a newly created Region in storage.
     *
     * @param CreateRegionRequest $request
     *
     * @return Response
     */
    public function store(CreateRegionRequest $request)
    {
        $input = $request->all();

        $region = $this->regionRepository->create($input);

        Flash::success('Region añadida satisfactoriamente.');

        return redirect(route('regions.index'));
    }

    /**
     * Display the specified Region.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $region = $this->regionRepository->find($id);

        if (empty($region)) {
            Flash::error('Region not found');

            return redirect(route('regions.index'));
        }

        return view('regions.show')->with('region', $region);
    }

    /**
     * Show the form for editing the specified Region.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        //$region = $this->regionRepository->find($id);
        $region = DB::table('Regiones')
            ->where('id', '=', $id)
            ->get();
        //dd($region);

        if (empty($region)) {
            Flash::error('Region no encontrada');

            return redirect(route('regions.index'));
        }

        return view('regions.edit')->with('regions', $region);
    }

    /**
     * Update the specified Region in storage.
     *
     * @param  int              $id
     * @param UpdateRegionRequest $request
     *
     * @return Response
     */
    public function update($id)
    {
        $input = \request()->validate([
            'Nombre' => 'required|string',
            'identificador' => 'required',
        ]);

        $region = DB::table('Regiones')->select('id', 'Nombre', 'identificador')->where('id', '=', $id)->get();

        if (empty($region)) {
            Flash::error('Region no encontrada');

            return redirect(route('regions.index'));
        }

        if ($input['Nombre'] != null) {
            $up = DB::table('Regiones')
                ->where('id', '=', $id)
                ->update(['Nombre' => $input['Nombre']]);
        }
        if ($input['identificador'] != null) {
            $up = DB::table('Regiones')
                ->where('id', '=', $id)
                ->update(['identificador' => $input['identificador']]);
        }

        Flash::success('Region actualizada satisfactoriamente.');

        return redirect(route('regions.index'));
    }

    /**
     * Remove the specified Region from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $region = DB::table('Regiones')
            ->where('id', '=', $id)
            ->first();

        if (empty($region)) {
            Flash::error('Region no encontrada');

            return redirect(route('regions.index'));
        }

        $sql = DB::table('Regiones')->where('id','=',$id)->delete();

        Flash::success('Region eliminada satisfactoriamente.');

        return redirect(route('regions.index'));
    }
}
