<?php

namespace App\Http\Controllers;

use App\Models\Operaciones;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Charts\SampleChart;
use App\Charts\UserChart;
use Illuminate\Support\Facades\DB;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $estacionamientos = Operaciones::select(DB::raw('sum(no_operaciones) as operacion, sum(ope_pasada) as Pasado, mes, ano'))
                                        ->groupBy('mes','ano')
                                        ->get();
        $Semana1 = Operaciones::Select(DB::raw('sum(no_operaciones) as operacion, mes'))
            ->whereBetween('dia', [1, 15])
            ->groupBy('mes')
            ->get();
        $Semana2 = Operaciones::Select(DB::raw('sum(no_operaciones) as operacion, mes'))
            ->whereBetween('dia', [16, 31])
            ->groupBy('mes')
            ->get();
        //dd($Semana1);

        return view('home.home')
            ->with('estacionamientos', $estacionamientos)
            ->with('semanas1', $Semana1)
            ->with('semanas2', $Semana2);

    }
}
