<?php

namespace App\Http\Controllers;

use App\DataTables\GerenteDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateGerenteRequest;
use App\Http\Requests\UpdateGerenteRequest;
use App\Repositories\GerenteRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Hash;
use Response;
use Illuminate\Support\Facades\DB;

class GerenteController extends AppBaseController
{
    /** @var  GerenteRepository */
    private $gerenteRepository;

    public function __construct(GerenteRepository $gerenteRepo)
    {
        $this->gerenteRepository = $gerenteRepo;
    }

    /**
     * Display a listing of the Gerente.
     *
     * @param GerenteDataTable $gerenteDataTable
     * @return Response
     */
    public function index(GerenteDataTable $gerenteDataTable)
    {
        $proyecto = DB::table('Gerentes')
            ->join('Proyecto', 'Proyecto.id', '=', 'Gerentes.id_proyecto')
            ->select('Gerentes.*', 'Proyecto.no_proyecto as No_proyecto', 'Proyecto.Nombre as ProyectoNombre')
            ->paginate(50);
        return view('gerentes.index')
            ->with('gerentes', $proyecto);
    }

    /**
     * Show the form for creating a new Gerente.
     *
     * @return Response
     */
    public function create()
    {
        $proyectos = DB::table('Proyecto')->select('id', 'no_proyecto', 'Nombre')->get();
        return view('gerentes.create')->with('proyectos', $proyectos);
    }

    /**
     * Store a newly created Gerente in storage.
     *
     * @param CreateGerenteRequest $request
     *
     * @return Response
     */
    public function store(CreateGerenteRequest $request)
    {
        $input = $request->all();

        $input['Password'] = Hash::make($input['Password']);
        $gerente = $this->gerenteRepository->create($input);

        Flash::success('Gerente añadido correctamente.');

        return redirect(route('gerentes.index'));
    }

    /**
     * Display the specified Gerente.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $gerente = $this->gerenteRepository->find($id);

        if (empty($gerente)) {
            Flash::error('Gerente not found');

            return redirect(route('gerentes.index'));
        }

        return view('gerentes.show')->with('gerente', $gerente);
    }

    /**
     * Show the form for editing the specified Gerente.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        //dd($id);
        $gerente = DB::table('Gerentes')
            ->join('Proyecto', 'Gerentes.id_proyecto', '=', 'Proyecto.id')
            ->select('Proyecto.no_proyecto', 'Proyecto.Nombre','Gerentes.id','Gerentes.Nombre', 'Gerentes.Email', 'Gerentes.Password')
            ->where('Gerentes.id', '=', $id)
            ->get();
        //dd($gerente);
        $proyectos = DB::table('Proyecto')->select('id', 'no_proyecto', 'Nombre')->get();

        if (empty($gerente)) {
            Flash::error('Gerente no encontrado');

            return redirect(route('gerentes.index'));
        }

        return view('gerentes.edit')->with('gerentes', $gerente)
            ->with('proyectos', $proyectos);
    }

    /**
     * Update the specified Gerente in storage.
     *
     * @param int $id
     * @param UpdateGerenteRequest $request
     *
     * @return Response
     */
    public function update($id)
    {
        //dd(request()->all());
        $input = \request()->validate([
            'id_proyecto' => 'integer',
            'Nombre' => 'string',
            'Email' => 'email',
            'Password' => '',
        ]);

        $Gerente = DB::table('Gerentes')->select('id', 'Nombre', 'Email', 'Password', 'id_proyecto')->where('id', '=', $id)->get();
        //dd($Gerente);
        //$gerente = $this->gerenteRepository->find($id);

        if (empty($Gerente)) {
            Flash::error('Gerente no encontrada');

            return redirect(route('gerentes.index'));
        }

        if ($input['id_proyecto'] != null) {
            $up = DB::table('Gerentes')
                ->where('id', '=', $id)
                ->update(['id_proyecto' => $input['id_proyecto']]);
        }
        if ($input['Nombre'] != null) {
            $up = DB::table('Gerentes')
                ->where('id', '=', $id)
                ->update(['Nombre' => $input['Nombre']]);
        }
        if ($input['Email'] != null) {
            $up = DB::table('Gerentes')
                ->where('id', '=', $id)
                ->update(['Email' => $input['Email']]);
        }
        if ($input['Email'] != null) {
            $up = DB::table('Gerentes')
                ->where('id', '=', $id)
                ->update(['Email' => $input['Email']]);
        }
        if ($input['Password'] != null) {
            $up = DB::table('Gerentes')
                ->where('id', '=', $id)
                ->update(['password' => Hash::make($input['Password'])]);
        }

        //$gerente = $this->gerenteRepository->update($request->all(), $id);

        Flash::success('Gerente actualizado correctamente.');

        return redirect(route('gerentes.index'));
    }

    /**
     * Remove the specified Gerente from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $gerente = DB::table('Gerentes')
            ->where('id','=', $id)
            ->first();
        //$gerente = $this->gerenteRepository->find($id);

        if (empty($gerente)) {
            Flash::error('Gerente no encontrado');

            return redirect(route('gerentes.index'));
        }

        $sql = DB::table('Gerentes')->where('id','=', $id)->delete();
        //$this->gerenteRepository->delete($id);

        Flash::success('Gerente eliminado.');

        return redirect(route('gerentes.index'));
    }
}
