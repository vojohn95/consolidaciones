<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Operaciones extends Model
{


    public $table = 'OperacionesDet';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'dia',
        'no_operaciones',
        'ope_pasada',
        'mes',
        'ano',
        'id_proyecto'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'dia' => 'integer',
        'no_operaciones' => 'integer',
        'ope_pasada' => 'integer',
        'mes' => 'integer',
        'ano' => 'integer',
        'id_proyecto' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'mes' => 'required',
        'ano' => 'required',
        'id_proyecto' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idProyecto()
    {
        return $this->belongsTo(\App\Models\Proyecto::class, 'id_proyecto');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function mes()
    {
        return $this->belongsTo(\App\Models\Me::class, 'mes');
    }
}
