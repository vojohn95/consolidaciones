<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Mes
 * @package App\Models
 * @version April 3, 2020, 7:26 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection operacionesDets
 * @property integer o
 * @property string Mes
 */
class Mes extends Model
{
    use SoftDeletes;

    public $table = 'Mes';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'o',
        'Mes'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'o' => 'integer',
        'Mes' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function operacionesDets()
    {
        return $this->hasMany(\App\Models\OperacionesDet::class, 'mes');
    }
}
