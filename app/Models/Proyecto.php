<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Proyecto
 * @package App\Models
 * @version April 3, 2020, 7:28 am UTC
 *
 * @property \App\Models\Regione idRegion
 * @property \Illuminate\Database\Eloquent\Collection gerentes
 * @property \Illuminate\Database\Eloquent\Collection operacionesDets
 * @property string Nombre
 * @property integer id_region
 */
class Proyecto extends Model
{


    public $table = 'Proyecto';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'no_proyecto',
        'Nombre',
        'id_region'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'Nombre' => 'string',
        'id_region' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_region' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idRegion()
    {
        return $this->belongsTo(\App\Models\Regione::class, 'id_region');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function gerentes()
    {
        return $this->hasMany(\App\Models\Gerente::class, 'id_proyecto');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function operacionesDets()
    {
        return $this->hasMany(\App\Models\OperacionesDet::class, 'id_proyecto');
    }
}
