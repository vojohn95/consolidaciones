<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Gerente
 * @package App\Models
 * @version April 3, 2020, 7:27 am UTC
 *
 * @property \App\Models\Proyecto idProyecto
 * @property string Nombre
 * @property string Email
 * @property string Password
 * @property integer id_proyecto
 */
class Gerente extends Model
{
    use SoftDeletes;

    public $table = 'Gerentes';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'Nombre',
        'Email',
        'Password',
        'id_proyecto'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'Nombre' => 'string',
        'Email' => 'string',
        'Password' => 'string',
        'id_proyecto' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'Nombre' => 'required',
        'Email' => 'required',
        'Password' => 'required',
        'id_proyecto' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idProyecto()
    {
        return $this->belongsTo(\App\Models\Proyecto::class, 'id_proyecto');
    }
}
