<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Operaciones;
use Faker\Generator as Faker;

$factory->define(Operaciones::class, function (Faker $faker) {

    return [
        'dia' => $faker->randomDigitNotNull,
        'no_operaciones' => $faker->randomDigitNotNull,
        'ope_pasada' => $faker->randomDigitNotNull,
        'mes' => $faker->word,
        'año' => $faker->word,
        'id_proyecto' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
