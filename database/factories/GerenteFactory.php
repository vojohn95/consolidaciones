<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Gerente;
use Faker\Generator as Faker;

$factory->define(Gerente::class, function (Faker $faker) {

    return [
        'Nombre' => $faker->word,
        'Email' => $faker->word,
        'Password' => $faker->word,
        'id_proyecto' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
