<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Mes;
use Faker\Generator as Faker;

$factory->define(Mes::class, function (Faker $faker) {

    return [
        'o' => $faker->randomDigitNotNull,
        'Mes' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
