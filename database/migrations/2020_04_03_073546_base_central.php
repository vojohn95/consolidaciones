<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BaseCentral extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Regiones', function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('Nombre')->nullable();
            $table->string('identificador')->nullable();
            $table->timestamps();
        });

        Schema::create('Proyecto', function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('Nombre', 45)->nullable();
            $table->unsignedBigInteger('id_region');
            $table->foreign('id_region')
                ->references('id')->on('Regiones');
            $table->timestamps();

        });

        Schema::create('Gerentes', function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('Nombre');
            $table->string('Email');
            $table->string('Password');
            $table->unsignedBigInteger('id_proyecto');
            $table->foreign('id_proyecto')
                ->references('id')->on('Proyecto');
            $table->timestamps();

        });

        Schema::create('Mes', function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('o')->nullable();
            $table->string('Mes', 45)->nullable();
            $table->timestamps();
        });

        Schema::create('OperacionesDet', function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('dia')->nullable();
            $table->integer('no_operaciones')->nullable();
            $table->unsignedBigInteger('mes');
            $table->unsignedBigInteger('id_proyecto');
            $table->foreign('mes')
                ->references('id')->on('Mes');
            $table->foreign('id_proyecto')
                ->references('id')->on('Proyecto');
            $table->timestamps();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('OperacionesDet');
        Schema::drop('Mes');
        Schema::drop('Gerentes');
        Schema::drop('Proyecto');
        Schema::drop('Regiones');
    }
}
